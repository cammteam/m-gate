package com.mteam.gate;

import com.mteam.gate.config.AppConfig;
import com.mteam.gate.controller.CashOutController;
import com.mteam.gate.controller.RechargeController;
import com.mteam.gate.controller.ReloadTokenManualController;
import com.mteam.gate.controller.WingCallBackController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        AppConfig appConfig = new AppConfig();
        appConfig.reloadConfig();
        register(RechargeController.class);
        register(CashOutController.class);
        register(ReloadTokenManualController.class);
        register(WingCallBackController.class);
    }

}
