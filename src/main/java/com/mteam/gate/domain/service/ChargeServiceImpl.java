package com.mteam.gate.domain.service;

import com.mteam.gate.common.AppUtils;
import com.mteam.gate.config.AppConfig;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.http.TokenGate1Manager;
import com.mteam.gate.domain.request.ChargeRequest;
import com.mteam.gate.domain.response.BaseResponse;
import com.mteam.gate.domain.response.ChargeResponse;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

//@Service
public class ChargeServiceImpl extends BaseSevice implements ChargeService {

    @Override
    public BaseResponse charge(ChargeRequest request) {

        int test = AppConfig.getConfig().getIntProperty("TEST", 1, "GATE1");
        if (test == 1) {

            ChargeResponse response = new ChargeResponse();
            response.setCode(0);
            response.setMessage("OK");
            response.setAmount(request.getTransAmount());
            response.setTransactionId(UUID.randomUUID().toString());
            return response;

        } else {

            String url = AppConfig.getConfig().getProperty("CHARGE_URL", "http://206.189.90.216:8686/payment/deposit", "GATE1");
            String username = AppConfig.getConfig().getProperty("USERNAME", "TEST50", "GATE1");
            String date = AppUtils.formatDate(new Date());
            String reqId = username + date;
            String processCode = AppConfig.getConfig().getProperty("CHARGE_WING_CODE", "DW0001", "GATE1");

            Map<String, String> headers = TokenGate1Manager.getInstance().getHeaderDefaultLoggedIn(username, reqId, processCode);
            JSONObject params = new JSONObject();
            params.put("account", request.getAccount());
            params.put("transAmount", request.getTransAmount() + "");
            params.put("tid", request.getTid());

            logger.info("=>charge url: {}, headers: {}, params: {}", url, headers, params);
            HttpSender sender = new HttpSenderImpl();
            JSONObject result = sender.post(url, headers, params);
            logger.info("=>charge url: {}, headers: {}, params: {}, result: {}", url, headers, params, result);

            ChargeResponse response = new ChargeResponse();
            if (result == null) {

                response.setCode(-1);
                response.setMessage("System busy, please try again");

            } else {

                int code = AppUtils.parseInt(result.get("code"));
                String transId = AppUtils.parseString(result.get("transId"));
                int transAmount = 0;

                if (code == 0) {
                    JSONObject depositResponse = (JSONObject) result.get("depositResponse");
                    if (depositResponse != null) {
                        transAmount = AppUtils.parseInt(depositResponse.get("transAmount"));
                    }
                }

                response.setCode(code);
                response.setMessage(AppUtils.parseString(result.get("message")));
                response.setTransactionId(transId);
                response.setAmount(transAmount);

            }

            response.setRaw(result);
            return response;

        }

    }

}
