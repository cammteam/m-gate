package com.mteam.gate.domain.service;


import com.mteam.gate.common.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AutoReloadTokenProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LoginService loginService;

    @Scheduled(fixedDelay = 10 * 60 * 1000)
    public void reloadToken() {
        logger.info("autoReloadToken at: ", AppUtils.formatDate(new Date(), "dd/MM/yyyy HH:mm:ss"));
        loginService.login();
    }

}
