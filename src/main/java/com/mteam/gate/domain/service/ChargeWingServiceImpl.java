package com.mteam.gate.domain.service;

import com.mteam.gate.config.AppConfig;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.request.ChargeRequest;
import com.mteam.gate.domain.response.BaseResponse;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ChargeWingServiceImpl extends BaseSevice implements ChargeService {

    @Override
    public BaseResponse charge(ChargeRequest request) {

        BaseResponse response = new BaseResponse();
        try {

            Map<String, String> params = new HashMap<>();
            params.put("default_wing", 1 + "");
            params.put("remember_card", 0 + "");
            params.put("wing_account", request.getAccount());
            params.put("sandbox", AppConfig.getConfig().getIntProperty("SANDBOX", 1, "WING") + "");
            params.put("amount", request.getTransAmount() + "");

            String username = AppConfig.getConfig().getProperty("USERNAME", "", "WING");
            String accessKey = AppConfig.getConfig().getProperty("REST_API_KEY", "", "WING");
            String billerCode = AppConfig.getConfig().getProperty("BILLER_CODE", "", "WING");
            String url = AppConfig.getConfig().getProperty("URL", "", "WING");

            String return_url = AppConfig.getConfig().getProperty("RETURN_URL", "http://test-gate.kohcard.club/m-gate/wing-callback", "WING");
            String remark = UUID.randomUUID().toString();

            params.put("username", username);
            params.put("rest_api_key", accessKey);
            params.put("biller_code", billerCode);
            params.put("return_url", return_url);
            params.put("remark", remark);

            logger.info("=>charge: url={}, params={}", url, params);

            HttpSender sender = new HttpSenderImpl();
            JSONObject resp = sender.postForm(url, null, params);
            logger.info("resp = {}", resp);
            if (resp == null) {
                response.setCode(-1);
                response.setMessage("Recharge failed !!!");
                return response;
            }

            response.setCode(1);
            response.setMessage("ប្រត្តិបត្តិការណ៍ របស់កំពុងត្រូវបានដំណើរការ សូមមេត្តារងចាំ!");

        } catch (Exception ex) {
            logger.error("", ex);
            response.setCode(-1);
            response.setMessage("Recharge failed !!!");
        }

        return response;

    }

}
