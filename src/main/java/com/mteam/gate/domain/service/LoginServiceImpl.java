package com.mteam.gate.domain.service;

import com.mteam.gate.common.AppUtils;
import com.mteam.gate.config.AppConfig;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.http.TokenGate1Manager;
import com.mteam.gate.domain.response.LoginResponse;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class LoginServiceImpl extends BaseSevice implements LoginService {

    private final AtomicInteger count = new AtomicInteger(0);

    @Override
    public LoginResponse login() {

//        String url = AppConfig.getConfig().getProperty("LOGIN_URL", "http://206.189.90.216:8686/auth/login", "GATE1");
//        String username = AppConfig.getConfig().getProperty("USERNAME", "TEST50", "GATE1");
//        String password = AppConfig.getConfig().getProperty("PASSWORD", "gD7vlQuIf5", "GATE1");
//        String key = AppConfig.getConfig().getProperty("KEY", "phvkarF63MAlZph/2qViEr9Ua7ojRZaUBcvS0FqnyAFYDH3W8+Ac0pi/Xe3LXPvckFqXqIoJ1iK/RH2XbBNDWOMryGa44JFrMBGLWDP/4g3jG2wGm/VtL7lyKtqVnaYUxJZ4MZRIEDwpv5LAX9QzXO2WyAF+WSGylXMRLjbdsHX4TUNiIqPiRAI50qyOMBEePET6NmXXTB9n9XxZ6uDMkgMvn1iyciXisbMXv/yHPAw0R/e0Zkps1sCvWTBVtAG//3+CWU4ZEPf2S43wwPPEpwIdnWmKrYgtRm6xYKXK4fBDyuuXgmngJxBwi+l4/E6sy0m7nVbN3lvIAeMkVAnFvsPZHZCpW3U8UBx+6kIL0DgHHQ8nBP/KExwjpzEovkRjWmh5uupLt6ia/4o58P7c6u2exiilrtZW/wP/5rAz2mFPvVL9VUnSNW8rAj5vyyfkAXeOmEfSQH+wDtfSwnGLJw==", "GATE1");
//        String date = AppUtils.formatDate(new Date());
//        String reqId = username + date;
//        String ip = AppConfig.getConfig().getProperty("IP", "45.77.45.43", "GATE1");
//
//        JSONObject params = new JSONObject();
//        params.put("userName", username);
//        params.put("password", password);
//        params.put("key", key);
//        params.put("ip", ip);
//        params.put("reqid", reqId);
//
//        HttpSender sender = new HttpSenderImpl();
//        Map<String, String> headers = TokenGate1Manager.getInstance().getHeaderDefault(reqId);
//        JSONObject result = sender.post(url, headers, params);
//        logger.info("=>login url: {}, headers: {}, params: {}, result: {}", url, headers, params, result);
//
//        boolean check = false;
//
//        if (result != null) {
//            String code = AppUtils.parseString(result.get("code"));
//            if ("00".equalsIgnoreCase(code)) {
//                String token = AppUtils.parseString(result.get("accessToken"));
//                TokenGate1Manager.getInstance().setToken(token);
//                check = true;
//                count.set(0);
//                logger.info("get token success : {}", token);
//            }
//        }
//
//        if (!check) {
//            int c = count.incrementAndGet();
//            logger.info("login failure count: {}", c);
//            if (c <= 10) {
//                login();
//            } else {
//                count.set(0);
//            }
//        }

        LoginResponse response = new LoginResponse();
        response.setCode(0);
        response.setMessage("");
//        response.setRaw(result);
        response.setRaw(null);
        return response;

    }

}
