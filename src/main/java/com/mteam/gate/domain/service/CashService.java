package com.mteam.gate.domain.service;

import com.mteam.gate.domain.request.CashRequest;
import com.mteam.gate.domain.response.BaseResponse;

public interface CashService {
    BaseResponse cash(CashRequest request);
}
