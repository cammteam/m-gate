package com.mteam.gate.domain.service;

import com.mteam.gate.domain.response.LoginResponse;

public interface LoginService {
    LoginResponse login();
}
