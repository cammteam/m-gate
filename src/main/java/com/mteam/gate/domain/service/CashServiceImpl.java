package com.mteam.gate.domain.service;

import com.mteam.gate.common.AppUtils;
import com.mteam.gate.config.AppConfig;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.http.TokenGate1Manager;
import com.mteam.gate.domain.request.CashRequest;
import com.mteam.gate.domain.response.BaseResponse;
import com.mteam.gate.domain.response.CashResponse;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class CashServiceImpl extends BaseSevice implements CashService {
    @Override
    public BaseResponse cash(CashRequest request) {

        String url = AppConfig.getConfig().getProperty("CASH_URL", "http://206.189.90.216:8686/payment/withdraw", "GATE1");
        String username = AppConfig.getConfig().getProperty("USERNAME", "TEST50", "GATE1");
        String date = AppUtils.formatDate(new Date());
        String reqId = username + date;
        String processCode = AppConfig.getConfig().getProperty("CASH_WING_CODE", "DW0001", "GATE1");

        Map<String, String> headers = TokenGate1Manager.getInstance().getHeaderDefaultLoggedIn(username, reqId, processCode);
        JSONObject params = new JSONObject();
        params.put("account", request.getAccount());
        params.put("transAmount", request.getTransAmount() + "");
        params.put("mobile", request.getMobile());
        params.put("fullName", request.getFullName());

        logger.info("=>cash url: {}, headers: {}, params: {}", url, headers, params);
        HttpSender sender = new HttpSenderImpl();
        JSONObject result = sender.post(url, headers, params);
        logger.info("=>cash url: {}, headers: {}, params: {}, result: {}", url, headers, params, result);

        CashResponse response = new CashResponse();
        response.setRaw(result);
        return response;

    }
}
