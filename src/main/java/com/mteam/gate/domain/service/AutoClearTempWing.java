package com.mteam.gate.domain.service;

import com.mteam.gate.common.AppUtils;
import com.mteam.gate.db.MySQLActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AutoClearTempWing {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Scheduled(fixedDelay = 10 * 60 * 1000)
    public void reloadToken() {
        logger.info("AutoClearTempWing at: ", AppUtils.formatDate(new Date(), "dd/MM/yyyy HH:mm:ss"));
        MySQLActions db = new MySQLActions();
        db.clearTemp();
    }

}
