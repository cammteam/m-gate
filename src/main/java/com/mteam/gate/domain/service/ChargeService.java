package com.mteam.gate.domain.service;

import com.mteam.gate.domain.request.ChargeRequest;
import com.mteam.gate.domain.response.BaseResponse;

public interface ChargeService {
    BaseResponse charge(ChargeRequest request);
}
