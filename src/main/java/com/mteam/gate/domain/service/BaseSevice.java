package com.mteam.gate.domain.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseSevice {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
