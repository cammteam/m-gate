package com.mteam.gate.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChargeRequest {
    @JsonProperty("wing_account")
    private String account;
    @JsonProperty("security_code")
    private String tid;
    @JsonProperty("amount")
    private int transAmount;
    @JsonProperty("account_id")
    private String accountId;
}
