package com.mteam.gate.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CashRequest {
    @JsonProperty("full_name")
    private String fullName;
    private String account;
    @JsonProperty("trans_amount")
    private String transAmount;
    private String mobile;
}
