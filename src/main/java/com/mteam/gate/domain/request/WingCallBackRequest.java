package com.mteam.gate.domain.request;

public class WingCallBackRequest {

    private String contents;

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "WingCallBackRequest{" +
                "contents='" + contents + '\'' +
                '}';
    }

}
