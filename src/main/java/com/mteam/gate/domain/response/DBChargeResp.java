package com.mteam.gate.domain.response;

public class DBChargeResp {

    private int result;
    private String detail;
    private String accountId;
    private String cdr;
    private long lastGold;
    private long realGold;

    public DBChargeResp() {
        this.result = -1;
        this.detail = "";
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCdr() {
        return cdr;
    }

    public void setCdr(String cdr) {
        this.cdr = cdr;
    }

    public long getLastGold() {
        return lastGold;
    }

    public void setLastGold(long lastGold) {
        this.lastGold = lastGold;
    }

    public long getRealGold() {
        return realGold;
    }

    public void setRealGold(long realGold) {
        this.realGold = realGold;
    }

    @Override
    public String toString() {
        return "DBChargeResp{" +
                "result=" + result +
                ", detail='" + detail + '\'' +
                ", accountId='" + accountId + '\'' +
                ", cdr='" + cdr + '\'' +
                ", lastGold=" + lastGold +
                ", realGold=" + realGold +
                '}';
    }

}
