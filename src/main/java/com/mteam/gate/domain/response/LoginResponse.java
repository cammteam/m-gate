package com.mteam.gate.domain.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.simple.JSONObject;

@Getter
@Setter
@ToString(callSuper = true)
public class LoginResponse extends BaseResponse {
    private String transId;
    private String resTime;
    private String accessToken;
    private String expireToken;
    private String remainTime;
    private JSONObject raw;
}
