package com.mteam.gate.domain.response;

public class WingCallBackResp {

    private String wingAccount;
    private String wingTranId;
    private String customerId;
    private String remark;
    private int amount;

    public String getWingAccount() {
        return wingAccount;
    }

    public void setWingAccount(String wingAccount) {
        this.wingAccount = wingAccount;
    }

    public String getWingTranId() {
        return wingTranId;
    }

    public void setWingTranId(String wingTranId) {
        this.wingTranId = wingTranId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "WingCallBackResp{" +
                "wingAccount='" + wingAccount + '\'' +
                ", wingTranId='" + wingTranId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", remark='" + remark + '\'' +
                ", amount=" + amount +
                '}';
    }

}
