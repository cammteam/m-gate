package com.mteam.gate.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.simple.JSONObject;

@Getter
@Setter
@ToString
public class ChargeResponse extends BaseResponse {
    private JSONObject raw;
    private int amount;
    @JsonProperty("transaction_id")
    private String transactionId;
}
