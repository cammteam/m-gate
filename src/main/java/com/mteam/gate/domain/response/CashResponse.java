package com.mteam.gate.domain.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.simple.JSONObject;

@Getter
@Setter
@ToString
public class CashResponse extends BaseResponse {
    private JSONObject raw;
}
