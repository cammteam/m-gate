package com.mteam.gate.domain.http;

import com.mteam.gate.config.AppConfig;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HttpSenderImpl extends HttpSender {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static CloseableHttpClient httpclientScratch;

    private synchronized static CloseableHttpClient getHttpClientScrachtInstance() {
        if (httpclientScratch == null) {
            int timeout = AppConfig.getConfig().getIntProperty("RECHARGE_TIMEOUT", 59000, "SETTINGS");
            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(timeout)
                    .setConnectTimeout(timeout)
                    .setConnectionRequestTimeout(timeout)
                    .build();
            httpclientScratch = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        }
        return httpclientScratch;
    }

    @Override
    public JSONObject get(String url, Map<String, String> headers, Map<String, String> params) {

        JSONObject result = null;
        String rs;

        try {
            HttpGet request = new HttpGet(url);
            List<NameValuePair> nvp = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                nvp.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            URI uri = new URIBuilder(request.getURI()).addParameters(nvp).build();
            request.setURI(uri);

            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(request);

            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
            logger.info("get result: {}", rs);
            if (rs != null && !"".equals(rs)) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }

        } catch (Exception ex) {
            logger.error("Exception: ", ex);
        }
        return result;
    }

    @Override
    public JSONObject post(String url, Map<String, String> headers, JSONObject params) {
        String res = "";
        JSONObject result = null;
        String rs;
        try {
            HttpPost post = new HttpPost(url);
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    post.addHeader(entry.getKey(), entry.getValue());
                }
            }
            StringEntity reqEntity = new StringEntity(params.toString(), StandardCharsets.UTF_8);
            post.setEntity(reqEntity);
            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(post);
            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
            res = rs;
            logger.info("postJson result: {}", rs);
            if (rs != null && rs.length() > 0) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }
        } catch (Exception ex) {
            logger.error("Exception: ", ex);
        }

        if (result == null) {
            result = new JSONObject();
        }
        result.put("raw_string", res);
        return result;
    }

    @Override
    public JSONObject postForm(String uri, Map<String, String> header, Map<String, String> params) {
        JSONObject result = null;
        String rs;
        try {
            HttpPost post = new HttpPost(uri);
            List<NameValuePair> nvp = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                nvp.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            post.setEntity(new UrlEncodedFormEntity(nvp, StandardCharsets.UTF_8));
            CloseableHttpResponse response = getHttpClientScrachtInstance().execute(post);
            HttpEntity entity = response.getEntity();
            rs = getStringResponse(entity);
            logger.info("postForm result: {}", rs);
            if (rs != null && !"".equals(rs)) {
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(rs);
            }
        } catch (Exception ex) {
            logger.error("Exception: ", ex);
        }
        return result;
    }

    private String getStringResponse(HttpEntity entity) throws IOException {
        String rs;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent(), StandardCharsets.UTF_8))) {
            char[] tmp = new char[1024];
            StringBuilder sb = new StringBuilder();
            int readBytes;
            while ((readBytes = br.read(tmp)) != -1) {
                sb.append(tmp, 0, readBytes);
            }
            rs = sb.toString();
        }
        return rs;
    }

}
