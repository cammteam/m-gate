package com.mteam.gate.domain.http;

import java.util.HashMap;
import java.util.Map;

public class TokenGate1Manager {

    private String token = "";

    private static TokenGate1Manager ourInstance = new TokenGate1Manager();

    public static TokenGate1Manager getInstance() {
        return ourInstance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private TokenGate1Manager() {
    }

    public Map<String, String> getHeaderDefault(String reqId) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("reqId", reqId);
        return headers;
    }

    public Map<String, String> getHeaderDefaultLoggedIn(String username, String reqId, String processCode) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put("x-user-name", username);
        headers.put("Authorization", token);
        //httppost.setHeader("Authorization", "Basic " + encoding);
        headers.put("reqId", reqId);
        headers.put("processcode", processCode);
        return headers;
    }

}
