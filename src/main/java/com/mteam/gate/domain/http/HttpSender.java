package com.mteam.gate.domain.http;

import org.json.simple.JSONObject;

import java.util.Map;

public abstract class HttpSender {
    public abstract JSONObject get(String url, Map<String, String> headers, Map<String, String> params);

    public abstract JSONObject post(String url, Map<String, String> headers, JSONObject params);

    public abstract JSONObject postForm(String uri, Map<String, String> header, Map<String, String> params);
}
