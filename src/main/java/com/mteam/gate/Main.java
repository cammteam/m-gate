package com.mteam.gate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        Logger logger = LoggerFactory.getLogger(Main.class);
        logger.info("---------------------STARTING APPLICATION--------------------------------");
        long maxBytes = Runtime.getRuntime().maxMemory();
        logger.info("----------->Max memory: " + maxBytes / 1024 / 1024 + "M");
    }
}
