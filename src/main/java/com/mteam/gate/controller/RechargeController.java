package com.mteam.gate.controller;

import com.mteam.gate.domain.request.ChargeRequest;
import com.mteam.gate.domain.response.BaseResponse;
import com.mteam.gate.domain.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Path("/charge")
public class RechargeController extends BaseController {

    @Autowired
    private ChargeService chargeService;

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BaseResponse charge(ChargeRequest request) {
        return chargeService.charge(request);
    }

}
