package com.mteam.gate.controller;

import com.mteam.gate.domain.request.CashRequest;
import com.mteam.gate.domain.response.BaseResponse;
import com.mteam.gate.domain.service.CashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Path("/cash")
public class CashOutController extends BaseController {

    @Autowired
    private CashService cashService;

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BaseResponse cash(CashRequest request) {
        return cashService.cash(request);
    }

}
