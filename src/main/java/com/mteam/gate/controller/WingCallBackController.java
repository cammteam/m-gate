package com.mteam.gate.controller;

import com.mteam.gate.common.AppUtils;
import com.mteam.gate.common.EncodeUtil;
import com.mteam.gate.common.MoneyFormat;
import com.mteam.gate.config.AppConfig;
import com.mteam.gate.db.MySQLActions;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.request.WingCallBackRequest;
import com.mteam.gate.domain.response.BaseResponse;
import com.mteam.gate.domain.response.DBChargeResp;
import com.mteam.gate.domain.response.WingCallBackResp;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@Path("/wing-callback")
public class WingCallBackController extends BaseController {

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BaseResponse callBack(WingCallBackRequest request) {
        logger.info("contents: {}", request);
        String accessKey = AppConfig.getConfig().getProperty("REST_API_KEY", "", "WING");
        String data;
        BaseResponse response = new BaseResponse();
        try {

            String contents = request.getContents();
            data = EncodeUtil.decryptWithKey(contents, accessKey);
            logger.info("data:" + data);

            if ("".equals(data)) {
                response.setCode(-1);
                response.setMessage("Data format incorrect");
                return response;
            }

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(data);
            if (json == null) {
                response.setCode(-1);
                response.setMessage("Cannot parse json data");
                return response;
            }

            MySQLActions db = new MySQLActions();
            WingCallBackResp resp = new WingCallBackResp();

            String amount = AppUtils.parseString(json.get("amount"));
            String[] g = amount.split(" ");
            int am = 0;
            if (g.length >= 2) {
                double f = AppUtils.parseDouble(g[1]);
                am = (int) f;
            }
            resp.setAmount(am);
            resp.setCustomerId(AppUtils.parseString(json.get("customer_name")));
            resp.setWingAccount(AppUtils.parseString(json.get("wing_account")));
            resp.setWingTranId(AppUtils.parseString(json.get("transaction_id")));
            resp.setRemark(AppUtils.parseString(json.get("remark")));
            DBChargeResp r = db.receiveWing(resp);
            logger.info("=>r = {}", r);

            if (r == null) {
                response.setCode(-1);
                response.setMessage("Receive wing response but, add balance failed !");
                return response;
            }

            if (r.getResult() != 0) {
                response.setCode(r.getResult());
                response.setMessage(r.getDetail());
                return response;
            }

            if (r.getRealGold() > 0) {
                HttpSender sender = new HttpSenderImpl();
                String urlNotiFy = AppConfig.getConfig().getProperty("URL_NOTIFY", "http://127.0.0.1:20999/omap/UpdateMoneySenderService", "SETTINGS");
                Map<String, String> params = new HashMap<>();
                params.put("acc_id", r.getAccountId());
                params.put("last_gold", r.getLastGold() + "");

                String msg = AppConfig.getConfig().getProperty("MSG_CHARGE", "អ្នកបានបញ្ចូល បាន ជោគជ័យ និង ទទួលបាន៖ $GOLD$ Kak", "SETTINGS");
                msg = msg.replaceAll("\\$GOLD\\$", MoneyFormat.format(r.getRealGold()));
                params.put("content", msg);

                sender.get(urlNotiFy, null, params);

            }

            response.setCode(0);
            response.setMessage("OK");

        } catch (Exception e) {
            logger.error("", e);
            response.setCode(-1);
            response.setMessage("System busy");
        }

        return response;

    }

    public static void main(String[] args) {
//        String t = DigestUtils.sha256Hex("123");
//        System.out.println("T = " + t);
        String t = "USD 5.00";
        String g[] = t.split(" ");
        int i = AppUtils.parseInt(g[1]);
        System.out.println("i = " + i);
        double f = AppUtils.parseDouble(g[1]);
        i = (int) f;
        System.out.println("i = " + i);
        System.out.println("f = " + f);
        System.out.println(Arrays.toString(g));
    }

}
