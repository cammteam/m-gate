package com.mteam.gate.db;

import com.kiemanh.vn.DbConnection;
import com.mteam.gate.domain.http.HttpSender;
import com.mteam.gate.domain.http.HttpSenderImpl;
import com.mteam.gate.domain.response.DBChargeResp;
import com.mteam.gate.domain.response.WingCallBackResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.Types;

public class MySQLActions {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public DBChargeResp receiveWing(WingCallBackResp request) {

        logger.info("=>receiveWing req={}", request);
        DBChargeResp response = new DBChargeResp();

        DbConnection conn = null;
        CallableStatement cs = null;
        String sql = "{call proc_commit_transaction_for_wing(?,?,?,?,?,?,?,?,?,?,?)}";

        try {
            conn = MySQLAccess.getInstance().getConn();
            cs = conn.prepareCall(sql);
            cs.setString(1, request.getRemark());
            cs.setString(2, request.getWingAccount());
            cs.setString(3, request.getCustomerId());
            cs.setString(4, request.getWingTranId());
            cs.setInt(5, request.getAmount());
            cs.registerOutParameter(6, Types.INTEGER); // rc
            cs.registerOutParameter(7, Types.VARCHAR); // rd
            cs.registerOutParameter(8, Types.VARCHAR); // acc_id
            cs.registerOutParameter(9, Types.BIGINT); // last balance
            cs.registerOutParameter(10, Types.BIGINT); // real Gold
            cs.registerOutParameter(11, Types.VARCHAR); // cdr
            boolean kq = cs.execute();
            if (!kq) {
                int result = cs.getInt(6);
                String detail = cs.getString(7);
                String accId = cs.getString(8);
                long lastGold = cs.getLong(9);
                long realGold = cs.getLong(10);
                String cdr = cs.getString(11);
                response.setAccountId(accId);
                response.setLastGold(lastGold);
                response.setCdr(cdr);
                response.setResult(result);
                response.setDetail(detail);
                response.setRealGold(realGold);
                conn.commit();
            } else {
                response.setResult(-1);
                response.setDetail("System error, please try again !");
                conn.rollback();
            }
        } catch (Exception ex) {
            logger.error("Error: ", ex);
            MySQLAccess.getInstance().rollback(conn);
            response.setResult(-1);
            response.setDetail("System busy");
        } finally {
            MySQLAccess.getInstance().closeCallableStatement(cs);
            MySQLAccess.getInstance().closeConn(conn);
        }
        logger.info("<=receiveWing req={}, res={}", request, response);
        return response;
    }

    public void clearTemp() {
        DbConnection conn = null;
        CallableStatement cs = null;
        String sql = "{call proc_clear_transaction_for_wing()}";

        try {
            conn = MySQLAccess.getInstance().getConn();
            cs = conn.prepareCall(sql);
            boolean kq = cs.execute();
            if (!kq) {
                conn.commit();
            } else {
                conn.rollback();
            }
        } catch (Exception ex) {
            logger.error("Error: ", ex);
            MySQLAccess.getInstance().rollback(conn);
        } finally {
            MySQLAccess.getInstance().closeCallableStatement(cs);
            MySQLAccess.getInstance().closeConn(conn);
        }
    }

}
