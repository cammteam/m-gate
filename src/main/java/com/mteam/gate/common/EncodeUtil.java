package com.mteam.gate.common;

import com.mteam.gate.config.AppConfig;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.omg.CORBA.portable.ApplicationException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncodeUtil {

    public static String encrypt(String value) {
//        try {
//            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
//            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
//
//            byte[] encrypted = cipher.doFinal(value.getBytes());
//            return Base64.encodeBase64String(encrypted);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        return null;
    }

    public static String decrypt(String encrypted, byte[] key) throws Exception {
//        String p = AppConfig.getConfig().getProperty("PASSWORD", "", "WING");
        byte[] k = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        IvParameterSpec iv = new IvParameterSpec(k);
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
        return new String(original);
    }

    public static String decryptWithKey(String orignalText, String strKey) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("SHA-256");
        final byte[] digestOfPassword = md.digest(strKey.getBytes(StandardCharsets.UTF_8));
        final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");
        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
        final byte[] plainTextBytes = Base64.decodeBase64(orignalText);
        final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);
        return new String(encodeTextBytes);
    }

    public static void main(String[] args) {
//        String accessKey = "6f89798127e4abc41827ebe778e44fbdd8377b342eb7e0a9d37b5d9189274e7f";
//        String ct = "5N6b54I+PnVm1CzewYY3TQuih8rfwTQiVIl54hgrZs66uxa/tl8DUjXh+dB2hZF8dALs5BBlaWphkcZ0oIRplajnTyIuIO0wEQLBH4tM9oNdEo33SidHbj8rZ9MpplzuuY4o8K3m7Y53qsLoZCDqlFHJjSPbBRnmK5Nt2/eW+3kNDFaDY4mMVocKMahxUVIsdyBLlQcmUxkmN37dpjhUMzKMqlX/NXMKSWQkAd2xvX6x9RXcKmZQGIuVr3iiJmTNFhYWSbTDsEjOwjKd0b6vt9NPjphLp8mylfpIae2qNPMnk8FEExHS10yUSkpHxcOehGg4e2Xj1WxF+h8W8giOPQ==";
//        System.out.println(decryptWithKey(ct, accessKey));
    }

}
