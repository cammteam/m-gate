package com.mteam.gate.common;


import java.text.NumberFormat;
import java.util.Locale;

public class MoneyFormat {

    public static final NumberFormat FORMATER = NumberFormat.getNumberInstance(Locale.GERMAN);

    public static String format(double vl) {
        return FORMATER.format(vl);
    }

    public static void main(String[] args) {
        long t = 12232352435l;
        System.out.print(format(t));
    }
}
