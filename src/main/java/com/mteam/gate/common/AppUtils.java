package com.mteam.gate.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppUtils {

    public static String parseString(Object object) {
        if (object == null) {
            return "";
        }
        return String.valueOf(object);
    }

    public static float parseFloat(Object object) {
        if (object == null) {
            return 0F;
        }
        float ret = 0F;
        try {
            ret = Float.parseFloat(parseString(object));
        } catch (Exception e) {
        }
        return ret;
    }

    public static double parseDouble(Object object) {
        if (object == null) {
            return 0D;
        }
        double ret = 0D;
        try {
            ret = Double.parseDouble(parseString(object));
        } catch (Exception e) {

        }
        return ret;
    }

    public static int parseInt(Object object) {
        if (object == null) {
            return 0;
        }
        int ret = 0;
        try {
            ret = Integer.parseInt(parseString(object));
        } catch (Exception ex) {

        }
        return ret;
    }

    public static short parseShort(Object object) {
        if (object == null) {
            return 0;
        }
        short ret = 0;
        try {
            ret = Short.parseShort(parseString(object));
        } catch (Exception ex) {

        }
        return ret;
    }

    public static long parseLong(Object object) {
        if (object == null) {
            return 0L;
        }
        long ret = 0L;
        try {
            ret = Long.parseLong(parseString(object));
        } catch (Exception ex) {

        }
        return ret;
    }

    public static String formatDate(Date date) {
        return formatDate(date, "yyyyMMddHHmmssSSS");
    }

    public static String formatDate(Date date, String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

}
