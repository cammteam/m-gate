package com.mteam.gate.config;

import com.mteam.gate.common.Common;
import org.jconfig.Configuration;
import org.jconfig.ConfigurationManager;
import org.jconfig.ConfigurationManagerException;
import org.jconfig.event.FileListener;
import org.jconfig.handler.XMLFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AppConfig {

    private static Configuration jConfig = null;
    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    public AppConfig() {
    }

    public synchronized static Configuration getConfig() {
        if (jConfig == null) {
            jConfig = ConfigurationManager.getConfiguration(Common.CONFIG_NAME);
        }
        return jConfig;
    }

    public static void reload() {
        synchronized (jConfig) {
            try {
                ConfigurationManager.getInstance().reload(Common.CONFIG_NAME);
                jConfig = ConfigurationManager.getConfiguration(Common.CONFIG_NAME);
            } catch (ConfigurationManagerException ex) {
                logger.error("reload()", ex);
            }
        }
    }

    public synchronized void reloadConfig() {
        logger.info("reload Config !!");
        ConfigurationManager cm = ConfigurationManager.getInstance();
        try {
            String fileStr = Common.CONFIG_FOLDER;
            File file = new File(fileStr);
            if (!file.exists()) {
                boolean createDir = file.mkdirs();
                logger.info("createDir: {} result: {}", fileStr, createDir);
            }
            fileStr += Common.CONFIG_FILE_NAME;
            file = new File(fileStr);
            if (!file.exists()) {
                createDefaultConfigFile(file);
            }
            XMLFileHandler fileHandler = new XMLFileHandler();
            fileHandler.setFile(file);
            FileListener fileListener = ConfigFileListener.getInstance(Common.CONFIG_NAME);
            fileHandler.addFileListener(fileListener);
            cm.load(fileHandler, Common.CONFIG_NAME);
        } catch (ConfigurationManagerException cme1) {
            logger.error("reloadConfig()", cme1);
        }
    }

    private void createDefaultConfigFile(File fout) {
        BufferedWriter w;
        try {
            w = new BufferedWriter(new FileWriter(fout));
            w.write("<?xml version=\"1.0\" ?>");
            w.newLine();
            w.write("\t<properties>");
            w.newLine();
            w.write("\t\t<category name=\"SETTINGS\">");
            w.newLine();
            w.write("\t\t\t<property name=\"AUTHORI_API\" value=\"http://103.63.109.153:8765/authentication/authorization\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"TRANSACTION_API\" value=\"http://35.201.238.143:8884/transaction\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"CHARGING_URL\" value=\"http://35.194.157.36:8884/transaction/charge\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"PUSH_URL\" value=\"http://35.194.157.36:8887/push/notify/user\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"ADMIN_PUSH_URL\" value=\"http://103.63.109.153:9933/reception/receive\"/>");
            w.newLine();
            w.write("\t\t</category>");
            w.newLine();
            w.write("\t\t<category name=\"MYSQL\">");
            w.newLine();
            w.write("\t\t\t<property name=\"URL\" value=\"jdbc:oracle:thin:@(description=(address=(host=192.168.11.121)(protocol=tcp)(port=1521))(connect_data=(SERVER = DEDICATED)(service_name=gstt)))\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"USERNAME\" value=\"AVMS\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"PASSWORD\" value=\"AVMS\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"MAX_LIMIT\" value=\"15\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"MIN_LIMIT\" value=\"5\"/>");
            w.newLine();
            w.write("\t\t\t<property name=\"AUTO_COMMIT\" value=\"false\"/>");
            w.newLine();
            w.write("\t\t</category>");
            w.newLine();
            w.write("\t</properties>");
            w.flush();
            w.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
